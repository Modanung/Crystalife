/* Cryst-(A)-Life
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



#include "effectmaster.h"
#include "inputmaster.h"
#include "player.h"
#include "spawnmaster.h"

#include "mastercontrol.h"

URHO3D_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context *context):
    Application(context),
    interval_{0.01f},
    untilStep_{1.0f},
    size_{42}
{
    instance_ = this;
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs")+"Crystalife.log";
    engineParameters_[EP_WINDOW_TITLE] = "Crystalife";
//    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Data;CoreData;Resources;";
}
void MasterControl::Start()
{
    context_->RegisterSubsystem(new EffectMaster(context_));
    context_->RegisterSubsystem(new InputMaster(context_));
    context_->RegisterSubsystem(new SpawnMaster(context_));

    CreateScene();

    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(MasterControl, HandleUpdate));
}
void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();
//    scene_->CreateComponent<Zone>()->SetAmbientColor(Color::GRAY);

    //Light
    Node* lightNode{ scene_->CreateChild("Light") };
    lightNode->SetPosition(Vector3(13.0f, 5.0f, 23.0f));
    lightNode->LookAt(Vector3::ZERO);
    Light* light{ lightNode->CreateComponent<Light>() };
    light->SetRange(128.0f);
    light->SetLightType(LIGHT_DIRECTIONAL);
    light->SetColor(Color(0.42f, 0.9f, 0.55f));

    //Camera
    Node* cameraNode{ scene_->CreateChild("Camera") };
    cameraNode->SetPosition(Vector3::ONE * size_ * 2.3f);
    cameraNode->LookAt(Vector3::ZERO);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    RENDERER->SetViewport(0, new Viewport(context_, scene_, camera));
    //Box!

    cellGroup_ = scene_->CreateComponent<StaticModelGroup>();
    cellGroup_->SetModel(CACHE->GetResource<Model>("Models/Cell.mdl"));

    assert(!(size_ % 2));

    for (unsigned x{0}; x < size_; ++x)
        for (unsigned y{0}; y < size_; ++y)
            for (unsigned z{0}; z < size_; ++z) {

                Node* node{scene_->CreateChild()};
                IntVector3 coords{static_cast<int>(x),
                                  static_cast<int>(y),
                                  static_cast<int>(z)};
                nodes_[coords] = node;

                node->SetPosition(Vector3(x * M_SQRT2,
                                          y * 2.0f - Sunken(coords),
                                          z * M_SQRT2));

                state_[coords] = false;
    }

    UpdateNodes();
}
bool MasterControl::Sunken(IntVector3 coords)
{
    return (coords.x_ % 2) != (coords.z_ % 2);
}

void MasterControl::Reset()
{
    for (IntVector3 coords : state_.Keys())
//            if (coords.Length() < size_ / 2)
            state_[coords] = !Random(static_cast<int>(coords.Length() + 1));

    UpdateNodes();
}

void MasterControl::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    untilStep_ -= eventData[Update::P_TIMESTEP].GetFloat();

    if (untilStep_ < 0.0f) {

        untilStep_ = interval_;

        Step();
    }

    Node* camera{ scene_->GetChild("Camera") };

    camera->Rotate(Quaternion(INPUT->GetMouseMoveY() * 0.05f,
                              INPUT->GetMouseMoveX() * 0.05f,
                              0.0f));
    camera->Translate(Vector3(INPUT->GetKeyDown(KEY_D) - INPUT->GetKeyDown(KEY_A),
                              0.0f,
                              INPUT->GetKeyDown(KEY_W) - INPUT->GetKeyDown(KEY_S)));

    if (INPUT->GetKeyPress(KEY_R)) {

        Reset();
    }
}
void MasterControl::Step()
{
    HashMap<IntVector3, bool> newState{ state_ };

    for (IntVector3 coords : newState.Keys()) {

        unsigned liveNeighbours{ CountNeighbours(coords) };

        if (state_[coords]) {

            if (liveNeighbours < 2 || liveNeighbours > 3)
                newState[coords] = false;
            else
                newState[coords] = true;

        } else {

            if (liveNeighbours == 3)
                newState[coords] = true;
            else
                newState[coords] = false;

        }

        ///Noise corner
//        if (coords.x_ < 6 && coords.y_ < 6 && coords.z_ < 6 && !newState[coords])
//            newState[coords] = Random(5 + static_cast<int>(Ceil(TIME->GetElapsedTime() * 23.0f)) % 100) == 0;
    }
    if (newState == state_)
        Reset();
    else {
        state_ = newState;

        UpdateNodes();
    }
}
unsigned MasterControl::CountNeighbours(IntVector3 coords)
{
    unsigned count{};

    for (unsigned n{0}; n < 8; ++n) {

        IntVector3 offset{};

        switch (n % 4) {
        case 0:
            offset.x_ = -1;
            if (coords.x_ == 0)
                offset.x_ += size_;
            break;
        case 1:
            offset.x_ = 1;
            if (coords.x_ + 1 == size_)
                offset.x_ -= size_;
            break;
        case 2:
            offset.z_ = -1;
            if (coords.z_ == 0)
                offset.z_ += size_;
            break;
        case 3:
            offset.z_ = 1;
            if (coords.z_ + 1 == size_)
                offset.z_ -= size_;
            break;
        default:
            break;
        }

        if (n > 3)
            offset.y_ = 1;

        if (Sunken(coords))
            offset += IntVector3::DOWN;

        if (coords.y_ == 0 && offset.y_ < 0)
            offset.y_ += size_;
        if (coords.y_ + 1 == size_ && offset.y_ > 0)
            offset.y_ -= size_;

        IntVector3 neighbourCoords{ coords + offset };
        bool alive{};
        if (state_.TryGetValue(neighbourCoords, alive)) {

            if (alive)
                ++count;
        }
    }
    return count;
}
void MasterControl::UpdateNodes()
{
    for (IntVector3 coords : state_.Keys()) {

        if (state_[coords])
            cellGroup_->AddInstanceNode(nodes_[coords]);
        else
            cellGroup_->RemoveInstanceNode(nodes_[coords]);
    }
}

void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}






Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}
Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerId){
            return p;
        }
    }
    return nullptr;
}
Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_){
        if (p->IsAlive()){

            if (!nearest
                    || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition(), pos) <
                        LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition(), pos)))
            {
                nearest = p;
            }
        }
    }
    return nearest;
}

